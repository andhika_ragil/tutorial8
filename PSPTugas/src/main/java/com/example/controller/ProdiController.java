package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.model.UnivModel;
import com.example.service.ProdiService;

@Controller
public class ProdiController {
	
	@Autowired
    ProdiService prodiDAO;
	
	// get prodi dengan peserta by kodeProdi
	@RequestMapping(value = "/prodi", method = RequestMethod.GET)
    public String viewProdi (Model model,
            @RequestParam(value = "kode", required = false) String kodeProdi)
    {
    	ProdiModel prodi = prodiDAO.selectProdi (kodeProdi);
    	if (prodi != null) {
    		List<PesertaModel> peserta = prodiDAO.selectPesertaByProdi(kodeProdi);
        	UnivModel univ = prodiDAO.selectUnivByProdi(prodi.getKode_univ());
        	
        	PesertaModel pesertaTermuda = prodiDAO.getPesertaByProdiTermuda(kodeProdi);
        	int pesertaTermudaUmur = prodiDAO.getPesertaByProdiTermudaUmur(kodeProdi);
        	
        	PesertaModel pesertaTertua = prodiDAO.getPesertaByProdiTertua(kodeProdi);
        	int pesertaTertuaUmur = prodiDAO.getPesertaByProdiTertuaUmur(kodeProdi);
    		
        	model.addAttribute ("prodi", prodi);
    		model.addAttribute ("peserta", peserta);
    		model.addAttribute ("univ", univ);
    		
    		model.addAttribute("termuda", pesertaTermuda);
    		model.addAttribute("termudaUmur", pesertaTermudaUmur);
    		
    		model.addAttribute("tertua", pesertaTertua);
    		model.addAttribute("tertuaUmur", pesertaTertuaUmur);
        	return "prodi-list";
        } else {
        	model.addAttribute ("kode", kodeProdi);
            return "prodi-not-found";
        }
    }
	
}
