package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.model.UnivModel;
import com.example.service.PesertaService;
import com.example.service.ProdiService;
import com.example.service.UnivService;

@Controller
public class PesertaController
{
    @Autowired
    PesertaService pesertaDAO;
    @Autowired
    ProdiService prodiDAO;
    @Autowired
    UnivService univDAO;

    @RequestMapping("/")
    public String index ()
    {
        return "index";
    }
    
    @RequestMapping(value = "/pengumuman/submit", method = RequestMethod.POST)
    public String pengumumanSubmit (Model model,
            @RequestParam(value = "nomor", required = false) String nomor)
    {
    	PesertaModel peserta = pesertaDAO.selectPeserta (nomor);
        // jika peserta ada
    	if (peserta != null) {
    		// cek apakah peserta lolos by kodeProdi
    		if(peserta.getKode_prodi() != null){
    			ProdiModel prodi = prodiDAO.selectProdi(peserta.getKode_prodi());
    	        UnivModel univ 	= univDAO.selectUniv(prodi.getKode_univ());
                model.addAttribute("pengumuman", "lolos");
                model.addAttribute("prodi", prodi);
                model.addAttribute("univ", univ); 
    		} else {
    			model.addAttribute("pengumuman", "tidak_lolos");
    		}
    		model.addAttribute("peserta", peserta);
    		int umur = pesertaDAO.selectPesertaUmur(peserta.getNomor());
            model.addAttribute("umur", umur);
            return "peserta-pengumuman";
        } else {
        	model.addAttribute ("nomor", nomor);
            return "peserta-not-found";
        }
    }
    
    @RequestMapping(value = "/peserta", method = RequestMethod.GET)
    public String viewPeserta (Model model,
            @RequestParam(value = "nomor", required = false) String nomor)
    {
    	PesertaModel peserta = pesertaDAO.selectPeserta (nomor);
        // jika peserta ada
    	if (peserta != null) {
    		model.addAttribute("peserta", peserta);
    		int umur = pesertaDAO.selectPesertaUmur(peserta.getNomor());
            model.addAttribute("umur", umur);
            return "peserta-detail";
        } else {
        	model.addAttribute ("nomor", nomor);
            return "peserta-not-found";
        }
    }
    
    //fitur lain
    @RequestMapping(value = "/peserta/remove", method = RequestMethod.GET)
    public String updatePesertaLolos (Model model,
            @RequestParam(value = "nomor", required = false) String nomor)
    {
    	// langsung delete
		pesertaDAO.removePeserta (nomor);
    	PesertaModel peserta = pesertaDAO.selectPeserta (nomor);
    	if (peserta != null) {
    		// cek apakah peserta lolos by kodeProdi
    		if(peserta.getKode_prodi() != null){
    			ProdiModel prodi = prodiDAO.selectProdi(peserta.getKode_prodi());
    	        UnivModel univ 	= univDAO.selectUniv(prodi.getKode_univ());
                model.addAttribute("pengumuman", "lolos");
                model.addAttribute("prodi", prodi);
                model.addAttribute("univ", univ); 
    		} else {
    			model.addAttribute("pengumuman", "tidak_lolos");
    		}
    		model.addAttribute("peserta", peserta);
    		int umur = pesertaDAO.selectPesertaUmur(peserta.getNomor());
            model.addAttribute("umur", umur);
            return "peserta-pengumuman";
        } else {
        	model.addAttribute ("nomor", nomor);
            return "peserta-not-found";
        }
    }
    
    // halaman 404
    @RequestMapping("/not-found")
    public String errorPage ()
    {
        return "not-found";
    }
}
