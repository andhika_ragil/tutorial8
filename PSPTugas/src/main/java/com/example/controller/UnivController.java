package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.model.PesertaModel;
import com.example.model.UnivModel;
import com.example.service.UnivService;

@Controller
public class UnivController {
	
	@Autowired
    UnivService univDAO;
	
	// halaman awal univ - menampilkan semua daftar universitas
	@RequestMapping("/univ")
    public String index (Model model)
    {
		List<UnivModel> univ= univDAO.selectAllUniv();
        model.addAttribute ("univ", univ);
        return "univ-index";
    }
	
	// menampilkan detail universitas beserta prodi
	@RequestMapping("/univ/{kodeUniv}")
	public String viewUniv (Model model,
            @PathVariable(value = "kodeUniv") String kodeUniv)
    {
        UnivModel univ = univDAO.selectUniv (kodeUniv);
        if (univ != null) {
            model.addAttribute ("univ", univ);
            return "univ-view";
        } else {
            model.addAttribute ("kodeUniv", kodeUniv);
            return "univ-not-found";
        }
    }
	
	// fitur lain
	// menampilkan daftar peserta berdasarkan univeristas tertentu
	@RequestMapping("/univ/{kodeUniv}/allpeserta")
    public String selectAllPesertaByUniv (Model model, 
    		@PathVariable(value = "kodeUniv") String kodeUniv)
    {
		UnivModel univ = univDAO.selectUniv (kodeUniv);
		List<PesertaModel> peserta = univDAO.selectAllPesertaByUniv(kodeUniv);
		model.addAttribute ("univ", univ);
        model.addAttribute ("peserta", peserta);
        return "univ-view-peserta";
    }
}
