package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.UnivMapper;
import com.example.model.PesertaModel;
import com.example.model.UnivModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UnivServiceDatabase implements UnivService
{
    @Autowired
    private UnivMapper univMapper;

    @Override
    public List<UnivModel> selectAllUniv ()
    {
        log.info ("select all university");
        return univMapper.selectAllUniv ();
    }
    
    @Override
    public UnivModel selectUniv (String kodeUniv)
    {
        log.info ("select univ with kodeUniv {}", kodeUniv);
        return univMapper.selectUniv (kodeUniv);
    }

	@Override
	public List<PesertaModel> selectAllPesertaByUniv(String kodeUniv) {
		log.info ("select peserta by univ with kodeUniv {}", kodeUniv);
        return univMapper.selectAllPesertaByUniv (kodeUniv);
	}

}
