package com.example.service;

import com.example.model.PesertaModel;

public interface PesertaService
{
    PesertaModel selectPeserta(String nomor);
    Integer selectPesertaUmur(String nomor);
	void removePeserta(String nomor);
}
