package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.PesertaMapper;
import com.example.model.PesertaModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PesertaServiceDatabase implements PesertaService
{
    @Autowired
    private PesertaMapper pesertaMapper;

    @Override
    public PesertaModel selectPeserta (String nomor)
    {
        log.info ("select student with npm {}", nomor);
        return pesertaMapper.selectPeserta (nomor);
    }

	@Override
	public Integer selectPesertaUmur(String nomor) {
		log.info ("select umur peserta with nomor {}", nomor);
        return pesertaMapper.selectPesertaUmur (nomor);
	}

	@Override
	public void removePeserta(String nomor) {
		log.info ("Delete peserta by nomor {}", nomor);
		pesertaMapper.removePeserta(nomor);
	}
    
}
