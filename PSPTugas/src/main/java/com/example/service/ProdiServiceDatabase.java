package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.ProdiMapper;
import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.model.UnivModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProdiServiceDatabase implements ProdiService
{
    @Autowired
    private ProdiMapper prodiMapper;
    
    @Override
    public ProdiModel selectProdi (String kodeProdi)
    {
        log.info ("select prodi with kodeProdi {}", kodeProdi);
        return (ProdiModel) prodiMapper.selectProdi (kodeProdi);
    }

	@Override
	public List<PesertaModel> selectPesertaByProdi(String kodeProdi) {
		log.info ("select peserta with kode_prodi {}", kodeProdi);
        return prodiMapper.selectPesertaByProdi (kodeProdi);
	}

	@Override
	public UnivModel selectUnivByProdi(String kodeUniv) {
		log.info ("select univ by prodi with prodi.kode_univ {}", kodeUniv);
        return prodiMapper.selectUnivByProdi (kodeUniv);
	}
	
	@Override
	public PesertaModel getPesertaByProdiTermuda(String kodeProdi) {
		log.info ("select peserta termuda by prodi with kodeProdi {}", kodeProdi);
        return prodiMapper.getPesertaByProdiTermuda (kodeProdi);
	}

	@Override
	public int getPesertaByProdiTermudaUmur(String kodeProdi) {
		log.info ("select umur peserta termuda by prodi with kodeProdi {}", kodeProdi);
        return prodiMapper.getPesertaByProdiTermudaUmur (kodeProdi);
	}
	
	@Override
	public PesertaModel getPesertaByProdiTertua(String kodeProdi) {
		log.info ("select peserta termuda by prodi with kodeProdi {}", kodeProdi);
        return prodiMapper.getPesertaByProdiTertua (kodeProdi);
	}

	@Override
	public int getPesertaByProdiTertuaUmur(String kodeProdi) {
		log.info ("select umur peserta termuda by prodi with kodeProdi {}", kodeProdi);
        return prodiMapper.getPesertaByProdiTertuaUmur (kodeProdi);
	}

	
}
