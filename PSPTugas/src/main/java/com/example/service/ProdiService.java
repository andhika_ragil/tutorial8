package com.example.service;

import java.util.List;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.model.UnivModel;

public interface ProdiService
{
    ProdiModel selectProdi (String kodeProdi);
    
	List<PesertaModel> selectPesertaByProdi(String kodeProdi);

	UnivModel selectUnivByProdi(String kode_univ);

	PesertaModel getPesertaByProdiTermuda(String kodeProdi);

	PesertaModel getPesertaByProdiTertua(String kodeProdi);

	int getPesertaByProdiTermudaUmur(String kodeProdi);

	int getPesertaByProdiTertuaUmur(String kodeProdi);

}
