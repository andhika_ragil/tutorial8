package com.example.service;

import java.util.List;

import com.example.model.PesertaModel;
import com.example.model.UnivModel;

public interface UnivService
{
    List<UnivModel> selectAllUniv ();
    UnivModel selectUniv (String kodeUniv);
	List<PesertaModel> selectAllPesertaByUniv(String kodeUniv);
}
