package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Many;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.model.UnivModel;

@Mapper
public interface UnivMapper
{ 
	// menampilkan semua universitas
    @Select("SELECT kode_univ, nama_univ FROM univ")
    List<UnivModel> selectAllUniv ();
    
    // menampilkan detail univ beserta daftar prodinya
    @Select("SELECT kode_univ, nama_univ, url_univ FROM univ WHERE kode_univ= #{kodeUniv}")
    @Results(value = {
    	    @Result(property="kode_univ", column="kode_univ"),
    	    @Result(property="nama_univ", column="nama_univ"),
    	    @Result(property="url_univ", column="url_univ"),
    	    @Result(property="prodi", column="kode_univ",
    		    javaType = List.class,
    		    many=@Many(select="selectProdiByUniv"))
    	    })
    UnivModel selectUniv (@Param("kodeUniv") String kodeUniv);
    
    // menampilkan daftar prodi by univ tertentu
    @Select("SELECT prodi.kode_univ, prodi.kode_prodi, prodi.nama_prodi " +
    		"FROM prodi JOIN univ " +
    		"ON prodi.kode_univ = univ.kode_univ " +
    		"where prodi.kode_univ = #{kodeUniv}")
    List<ProdiModel> selectProdiByUniv (@Param("kodeUniv") String kodeUniv);

    // menampilkan semua peserta by univ
    @Select("SELECT peserta.*, prodi.nama_prodi " +
    		"FROM prodi JOIN peserta " +
    		"ON peserta.kode_prodi = prodi.kode_prodi " +
    		"JOIN univ " +
    		"ON univ.kode_univ = prodi.kode_univ " +
    		"where univ.kode_univ = #{kodeUniv}")
	List<PesertaModel> selectAllPesertaByUniv(String kodeUniv);
}
