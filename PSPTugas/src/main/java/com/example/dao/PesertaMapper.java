package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.model.PesertaModel;

@Mapper
public interface PesertaMapper
{ 
	// menampilkan pengumuman peserta beserta nama prodi dan unversitasnya 
	@Select("SELECT nomor, nama, tgl_lahir, kode_prodi FROM peserta where nomor = #{nomor}")
    PesertaModel selectPeserta (@Param("nomor") String nomor);
	
	@Select("SELECT TIMESTAMPDIFF(YEAR, tgl_lahir, CURDATE()) AS umur FROM peserta WHERE nomor = #{nomor}")
    Integer selectPesertaUmur(String nomor);
    
//	@Select("SELECT kode_univ, kode_prodi, nama_prodi FROM prodi WHERE kode_prodi = #{kodeProdi}")
//    @Results(value = {
//        @Result(property = "kode_univ", column = "kode_univ"),
//        @Result(property = "kode_prodi", column = "kode_prodi"),
//        @Result(property = "nama_prodi", column = "nama_prodi"),
//        @Result(property = "pesertas", column = "kode_prodi", 
//        	javaType = List.class, 
//        	many=@Many(select = "selectManyPeserta"))
//        })
//    ProdiModel selectProdi (@Param("kodeProdi") String kodeProdi);
    
    @Select("SELECT peserta.nomor, peserta.nama, peserta.tgl_lahir, peserta.kode_prodi FROM prodi " + 
    		" JOIN peserta ON prodi.kode_prodi = peserta.kode_prodi " + 
    		" WHERE prodi.kode_prodi = #{kode_prodi} " + 
    		" ORDER BY peserta.tgl_lahir ASC")
    List<PesertaModel> selectPesertaByProdi(@Param("kode_prodi") String kode_prodi);

    @Update("UPDATE peserta SET kode_prodi = NULL WHERE nomor = #{nomor}")
	void removePeserta(@Param("nomor")String nomor);
}
