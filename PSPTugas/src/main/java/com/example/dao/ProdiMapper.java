package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Many;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.model.UnivModel;

@Mapper
public interface ProdiMapper
{   
    // select prodi
    @Select("SELECT kode_univ, kode_prodi, nama_prodi FROM prodi WHERE kode_prodi = #{kode_prodi}")
    @Results(value = {
        @Result(property = "kode_univ", column = "kode_univ"),
        @Result(property = "kode_prodi", column = "kode_prodi"),
        @Result(property = "nama_prodi", column = "nama_prodi"),
        @Result(property = "peserta", column = "kode_prodi",
        	javaType = List.class, 
        	many = @Many(select = "selectPesertaByProdi"))
        })
    ProdiModel selectProdi(@Param("kode_prodi") String kode_prodi);
    
    // select peserta yang mengikuti prodi tersebut
    @Select("SELECT peserta.nomor, peserta.nama, peserta.tgl_lahir, peserta.kode_prodi FROM prodi " + 
    		" JOIN peserta ON prodi.kode_prodi = peserta.kode_prodi " + 
    		" WHERE prodi.kode_prodi = #{kode_prodi} " + 
    		" ORDER BY peserta.tgl_lahir ASC")
    List<PesertaModel> selectPesertaByProdi(@Param("kode_prodi") String kode_prodi);
    
    // select univ yang memiliki prodi tersebut
    @Select("SELECT kode_univ, nama_univ, url_univ FROM univ WHERE kode_univ = #{kode_univ}")
	UnivModel selectUnivByProdi(@Param("kode_univ") String kodeUniv);
    
    // select Data Peserta Tertua
    @Select("SELECT nomor, nama, tgl_lahir, TIMESTAMPDIFF(YEAR, tgl_lahir, CURDATE()) AS umur " + 
    		" FROM peserta " + 
    		" WHERE kode_prodi = #{kode_prodi} ORDER BY tgl_lahir ASC LIMIT 1")
	PesertaModel getPesertaByProdiTertua(@Param("kode_prodi") String kodeProdi);
    
    @Select("SELECT TIMESTAMPDIFF(YEAR, tgl_lahir, CURDATE()) AS umur " + 
    		" FROM peserta " + 
    		" WHERE kode_prodi = #{kode_prodi} ORDER BY tgl_lahir ASC LIMIT 1")
	int getPesertaByProdiTertuaUmur(String kodeProdi);
    
    // select Data Peserta Termuda
    @Select("SELECT nomor, nama, tgl_lahir, TIMESTAMPDIFF(YEAR, tgl_lahir, CURDATE()) AS umur " + 
    		" FROM peserta " + 
    		" WHERE kode_prodi = #{kode_prodi} ORDER BY tgl_lahir DESC LIMIT 1")
	PesertaModel getPesertaByProdiTermuda(@Param("kode_prodi") String kodeProdi);
    
    @Select("SELECT TIMESTAMPDIFF(YEAR, tgl_lahir, CURDATE()) AS umur " + 
    		" FROM peserta " + 
    		" WHERE kode_prodi = #{kode_prodi} ORDER BY tgl_lahir DESC LIMIT 1")
	int getPesertaByProdiTermudaUmur(String kodeProdi);
    
    
}
